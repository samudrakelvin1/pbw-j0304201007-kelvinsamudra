<?php include 'header.php'; ?>

        <h1 class="mt-3 mb-3">MIDGARD</h1>

        <?php if (!empty($_SESSION['username'])) { ?>

        <a href="formMahasiswa.php" colspan="8" class="btn btn-info btn-sm mb-3">+</a>
        
        <?php } ?>

        <table class="table">
            <thead class="table-info">
                <tr>
                    <th>No.</th>
                    <th>NIM</th>
                    <th>Nama</th>
                    <th>Jenis Kelamin</th>
                    <th>Tanggal Lahir</th>
                    <th>Alamat</th>
                    <th>Program Studi</th>

                    <?php if (!empty($_SESSION['username'])) { ?>

                    <th>Aksi</th>

                    <?php } ?>

                </tr>
            </thead>
            <tbody>
                    
                <?php
                    $sql = 'SELECT * FROM mahasiswa JOIN prodi ON prodi.id_prodi = mahasiswa.id_prodi';
                        
                    $query = mysqli_query($conn, $sql);

                    $i = 1;

                    while ($row = mysqli_fetch_object($query)) {
                ?>
                    
                <tr>
                    <td><?php echo $i++.'.'; ?></td>
                    <td><?php echo $row->nim; ?></td>
                    <td><?php echo $row->nama; ?></td>
                    <td><?php echo $row->jenis_kelamin; ?></td>
                    <td><?php echo $row->tanggal_lahir; ?></td>
                    <td><?php echo $row->alamat; ?></td>
                    <td><?php echo $row->nama_prodi; ?></td>

                    <?php if (!empty($_SESSION['username'])) { ?>

                    <td>
                        <a href="formMahasiswa.php?nim=<?php echo $row->nim; ?>" class="btn btn-sm btn-warning">Ubah</a> 
                        <a href="deleteMahasiswa.php?nim=<?php echo $row->nim; ?>" class="btn btn-danger btn-sm" onclick="return confirm('Apakah anda yakin ingin menghapus data?');">
                            Hapus
                        </a>
                    </td>

                    <?php } ?>
                </tr>

                <?php
                        }

                    if (! mysqli_num_rows($query)) {
                        echo '<tr><td colspan="8" class="text-center">Unknown.</td></tr>';
                   }
                ?>

            </tbody>
        </table>

<?php include 'footer.php'; ?>
