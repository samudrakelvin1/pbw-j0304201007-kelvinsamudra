<?php include 'header.php';


$edit = false;

if (!empty($_GET['id_prodi'])) {
    $sql = 'SELECT * FROM prodi WHERE id_prodi="' . $_GET['id_prodi'] . '"';
    $query = mysqli_query($conn, $sql);

    if (mysqli_num_rows($query)) {
        $edit = true;
        $data = mysqli_fetch_object($query);
    }
}

?>

<h1>Form Program Studi</h1>

<form method="post" action="saveProgramStudi.php">

    <!-- <div class="mb-3">
        <label class="form-label">ID Prodi</label>
        <input type="text" class="form-control" placeholder="ID" name="idProdi" required>
    </div> -->
    <div class="mb-3">
        <label class="form-label">Nama Prodi</label>
        <input type="text" class="form-control" placeholder="Nama Prodi" value="<?php if ($edit) echo $data->nama_prodi ?>" name="namaProdi" required>
        <input type="hidden" name="id_prodi" value="<?php if ($edit) echo $data->id_prodi; ?>">

    </div>

    <div class="mb-3">
        <input type="submit" class="btn btn-success" value="Simpan">
    </div>

</form>

<?php include 'footer.php'; ?>